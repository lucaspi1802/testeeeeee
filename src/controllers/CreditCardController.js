const { response } = require('express');
const CreditCard = require('../models/CreditCard');
const User= require('../models/CreditCard');

const create = async(req, res) => {

    try{
        const creditcard = await CreditCard.create(req.body);
        console.log(creditcard);
        return res.status(201).json({message: "Cartão de crédito cadastrado com sucesso!", creditcard});
    }catch(err){
        res.status(500).json({error: err});
    }
};

const index = async(req, res) => {
    try{
        const creditcards = await CreditCard.findAll();
        return res.status(200).json({creditcards});
    }catch(err) {
        return res.status(500).json({err});
    }
    };

const show = async(req, res) => {
    const {id} = req.params;
    try {
        const creditcard = await CreditCard.findByPk(id);
        return res.status(200).json({creditcard});
    }catch(err){
        return res.status(500).json({err});
    }
};

const update = async(req, res) => {
    const {id} = req. params;
    try {
        const [updated] = await CreditCard.update(req.body, {where: {id: id}}); 
        if(updated) {
            const creditcard = await CreditCard.findByPk(id);
            return res.status(200).send(creditcard);
        }
        throw new Error();
    }catch(err){
        return res.status(500).json("Cartão de crédito não encntrado");
    }
};

const destroy = async(req, res) => {
    const {id} = req.params;
    try{
        const deleted = await CreditCard.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Cartão de crédito deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Cartão de crédito não encontrado.");
    }
};

const addRelationship = async(req, res) => {
    const {id} = req.params;
    try{
        const creditcard = await CreditCard.findByPk(id);
        const user = await User.findByPk(req.body.UserId);
        await creditcard.setUser(user);
        return res.status(200).json(user);
    }catch(err){
        return res.status(500).json({err});
    }
};

const removeRellationship = async(req, res) => {
    const {id} = req.params;
    try{
        const creditcard = await CreditCard.findByPk(id);
        await creditcard.setSeller(null);
        return res.status(200).json(creditcard);
    }catch(err){
        return res.status(500).json({err});
    }
};


module.exports = {
    create,
    index,
    show,
    update,
    destroy,
    addRelationship,
    removeRellationship,
};