const DataTypes = require('sequelize');
const sequelize = require('../config/sequelize');

const CreditCard = sequelize.define('CreditCard', {

    number:{
        type: DataTypes.STRING,
        allowNull: false
    },

    expirationdate:{
        type: DataTypes.STRING,
        allowNull: false
    },

    securitycode:{
        type: DataTypes.STRING,
        allowNull: false
    },
});

CreditCard.associate = function(models) {
    CreditCard.belongsTo(models.User)
};

module.exports = CreditCard;