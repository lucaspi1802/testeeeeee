const { response } = require('express');
const Product = require('../models/Product');
const User = require('../models/User');

const create = async(req, res) => {
    
    try{
        const product = await Product.create(req.body);
        return res.status(201).json({message: "Produto cadastrado om sucesso!", product});
    }catch(err){
        res.status(500).json({error: err});
    }
};

const index = async(req, res) => {
    try{
        const products = await Product.findAll();
        return res.status(200).json({products});
    }catch(err) {
        return res.status(500).json({err});
    }
    };

const show = async(req, res) => {
    const {id} = req.params;
    try {
        const product = await Product.findByPk(id);
        return res.status(200).json({product});
    }catch(err){
        return res.status(500).json({err});
    }
};

const update = async(req, res) => {
    const {id} = req. params;
    try {
        const [updated] = await Product.update(req.body, {where: {id: id}}); 
        if(updated) {
            const product = await Product.findByPk(id);
            return res.status(200).send(product);
        }
        throw new Error();
    }catch(err){
        return res.status(500).json("Produto não encntrado");
    }
};

const destroy = async(req, res) => {
    const {id} = req.params;
    try{
        const deleted = await Product.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Produto deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Produto não encontrado.");
    }
};

const addBuyer = async(req, res) => {
    const {id} = req.params;
    try{
        const product = await Product.findByPk(id);
        const buyer = await User.findByPk(req.body.buyerId);
        await product.setBuyer(buyer);
        return res.status(200).json(product);
    }catch(err){
        return res.status(500).json({err});
    }
};

const removeBuyer = async(req, res) => {
    const {id} = req.params;
    try{
        const product = await Product.findByPk(id);
        await product.setBuyer(null);
        return res.status(200).json(product);
    }catch(err){
        return res.status(500).json({err});
    }
};

const addSeller = async(req, res) => {
    const {id} = req.params;
    try{
        const product = await Product.findByPk(id);
        const seller = await User.findByPk(req.body.sellerId);
        await product.setSeller(seller);
        return res.status(200).json(product);
    }catch(err){
        return res.status(500).json({err});
    }
};

const removeSeller = async(req, res) => {
    const {id} = req.params;
    try{
        const product = await Product.findByPk(id);
        await product.setSeller(null);
        return res.status(200).json(product);
    }catch(err){
        return res.status(500).json({err});
    }
};

module.exports = {
    create,
    index,
    show,
    update,
    destroy,
    addBuyer,
    removeBuyer,
    addSeller,
    removeSeller, 
};